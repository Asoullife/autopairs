const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const mysql = require("mysql2/promise");
const path = require("path");
const render = require("koa-ejs");
const koaBody = require("koa-body");
const pool = require("./db");

const app = new Koa();
const router = new Router();

app.use(async (ctx, next) => {
  ctx.test = "Hello";
  await next();
});

render(app, {
  root: path.join(process.cwd(), "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false
});

app.use(koaBody({ multipart: true }));

router.get("/index", async (ctx, next) => {
  const data = { title: "index", css: "css/style.css"};
  await ctx.render("index", data);
  await next;
});

router.post("/completed", async (ctx, next) => {
  ctx.redirect("/index");
  await next;
});

router.get("/", async (ctx, next) => {
  await pool.query(`UPDATE orderlist set hub = null`)
  await pool.query(`DELETE from shipping`)
  const data = { title: "login", css: "css/style.css" };
  await ctx.render("login",data)
  await next;
});

router.get("/po_list", async (ctx, next) => {

  const data = { title: "PO List", css: "css/style.css"};
  await ctx.render("po_list", data);
  await next;
});

router.get('/saleslist', async (ctx, next) => {
  const [rows] = await pool.query(`SELECT distinct SO, employee, order_date FROM orderlist WHERE hub is null`)
  const [slrows] = await pool.query(`SELECT distinct SO, employee, order_date, hub FROM orderlist WHERE hub is not null`)
  const [rowCount] = await pool.query(`select count(distinct SO) as a from orderlist where hub is null`)
  const data ={title : 'Salelist',css : 'css/style2.css', rows, slrows, rowCount};
  await ctx.render('saleslist',data)
  await next
})

router.post('/saleslist', async (ctx, next) => {
  //consoleconsole.log(ctx.request.body)
  const hub = ctx.request.body.orderHub
  const so = ctx.request.body.orderSo
  await pool.query(`UPDATE orderlist SET hub=? WHERE SO=?`, [hub, so])
  const [completed] = await pool.query(`SELECT distinct SO, employee, order_date,hub FROM orderlist WHERE SO=?`,[so])
  const [rows] = await pool.query(`SELECT distinct SO, employee, order_date FROM orderlist WHERE hub is null`)
  const [slrows] = await pool.query(`SELECT distinct SO, employee, order_date, hub FROM orderlist WHERE hub is not null`)
  const [update] = await pool.query(`INSERT INTO shipping 
    (Sale_Order,Customer_Name,Status,Date_In,Date_Out,Hub) VALUES (?,?,?,?,?,?)`, 
    [completed[0].SO,completed[0].employee,"Is Being Process",completed[0].order_date,null,completed[0].hub])
  const data ={title : 'Salelist',css : 'css/style2.css', rows, slrows};
  ctx.redirect('saleslist')
  //await ctx.render('saleslist',data)
  await next
})

router.get("/hub", async (ctx, next) => {
  const [rows, fields] = await pool.query(`SELECT * FROM hub_store`);

  const [rowss] = await pool.query(`SELECT Hub FROM shipping where Status != ?`,["Completed"]);
  let count = []
  for (let i = 0 ; i<rowss.length;i++){
    
    let name = rowss[i].Hub;
    if (isNaN(count[name])){
      count[name] = 1;
    }
    else{
      count[name]++;
    }
  }
  const data = { title: "Hub", css: "css/hub.css", rows,count};
  await ctx.render("hub", data);
  await next;
});

router.post("/hubadd", async (ctx, next) => {
    const { name, phone, address } = ctx.request.body;
    await pool.query(`
    insert into hub_store
        (Name,Tel,Address)
    values
        (?,?,?)
    `,
      [name, phone, address]
    );
    ctx.redirect("/hub");
  await next;
});

router.post("/hubdel", async (ctx, next) => {
  const {del} = ctx.request.body;
  await pool.query(
    `
  delete from hub_store
  where name=?
  `,
    [del]
  );
  const [rows] = await pool.query(`SELECT Sale_Order,Hub from shipping where Status != ?`,["Completed"])
  for (let i in rows){
    if (rows[i].Hub===del){
       await pool.query(`delete from shipping where (Hub = ?) and (Status!= ?)`,[del,"Completed"])
       await pool.query(`UPDATE orderlist SET hub=? WHERE (hub=?) and (SO = ?)`, [null, del,rows[i].Sale_Order])
       
    }

  }
  
  ctx.redirect("/hub");
  await next;
});

router.post("/hublist", async (ctx, next) => {
  const {name,completed,names} = ctx.request.body
  //console.log(ctx.request.body)
  let data = {}
  if (completed!==undefined){
    //console.log(ctx.request.body)
    let = time = new Date();
    let month = time.getMonth()+1
    time = `${time.getDate()}/${month}/${time.getFullYear()}`
    await pool.query(`UPDATE shipping set Status = ? , Date_Out = ? WHERE Sale_Order = ?`,
    ["Completed",time,completed])
    const [rowsCompleted] = await pool.query(`SELECT Sale_Order,Customer_Name,Status,Date_In,Date_Out 
    FROM shipping where (Hub = ?) and (Status = ?)`,[names,"Completed"])
    const [rows] = await pool.query(`SELECT Sale_Order,Customer_Name,Status,Date_In,Date_Out 
    FROM shipping where (Hub = ?) and (Status != ?)`,[names,"Completed"])
    //await pool.query(`UPDATE orderlist SET Status=? WHERE SO=?`, ["Completed", completed])
    data = { title: "Hublist", css: "css/hublist.css",rows,rowsCompleted,names};
  }
  else if (name!==undefined){
    const names = ctx.request.body;
    const [rowsCompleted] = await pool.query(`SELECT Sale_Order,Customer_Name,Status,Date_In,Date_Out 
    FROM shipping where (Hub = ?) and (Status = ?) `,[name,"Completed"])
    const [rows] = await pool.query(`SELECT Sale_Order,Customer_Name,Status,Date_In,Date_Out
    FROM shipping where (Hub = ?) and (Status != ?) `,[name,"Completed"])
    data = {title: "Hublist", css: "css/hublist.css",rows,rowsCompleted,names};
  }
  await ctx.render("hublist",data)
});

router.post('/order',async (ctx,next) => {
  const sale = ctx.request.body.saleID
  const [rows, fields] = await pool.query(`SELECT * FROM orderlist where SO=?`, sale)
  const [rows2] = await pool.query(`SELECT sum(unit*price) as total_price FROM orderlist where SO=?`, sale)
  const [rows3] = await pool.query(`SELECT Name FROM hub_store`)
  const [rowCount] = await pool.query(`select count(distinct SO) as a from orderlist where hub is null`)
  //console.log(rowCount)
  const data ={title : 'Order',css : 'css/style2.css', rows, rows2, rows3, rowCount}

  await ctx.render('order', data)
})

router.get('/stock', async (ctx, next) => {
  const [rows, fields] = await pool.query(`SELECT * FROM main_stock WHERE NO <= 1000`);
  //console.log(rows)
  let row = []
  for (let i in rows){
    for (let j in rows[i]){
        row.push(j)
    }
    if (i==0){
      break
    }
  }
  const data ={title : 'Stock',css : 'css/style5.css',row,rows};
  await ctx.render('stock',data)
  await next
})

app.use(serve(path.join(process.cwd(), "public")));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
